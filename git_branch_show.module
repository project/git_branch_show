<?php

/**
 * @file
 * Contains git_branch_show.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function git_branch_show_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the git_branch_show module.
    case 'help.page.git_branch_show':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Display the current git branch in the admin toolbar') . '</p>';
      return $output;

    default:
  }
}


/**
 * Implements hook_toolbar().
 */
function git_branch_show_toolbar() {
  if ($branch_name = get_git_branch()) {
    // Add a toolbar item that indicates the current git branch.
    $items['git-branch'] = [
      '#type' => 'toolbar_item',
      '#weight' => 999,
      '#cache' => [
        'contexts' => ['route'],
      ],
      'tab' => [
        '#type' => 'inline_template',
        '#template' => '<div class="toolbar-check" href="{{ more_info_link }}">Current Git Branch: {{ branch_name }}</div>',
        '#context' => [
          'more_info_link' => 'https://www.drupal.org/project/git_branch_show',
          'branch_name' => $branch_name,
        ],
        '#attached' => [
          'library' => ['git_branch_show/toolbar-check'],
        ],
      ],
    ];
    return $items;
  }
}

/**
 * Returns the current git branch or false if not a git repo.
 *
 * @return string|bool
 */
function get_git_branch() {
  $shellOutput = [];
  exec('git branch | ' . "grep ' * '", $shellOutput);
  foreach ($shellOutput as $line) {
    if (strpos($line, '* ') !== false) {
      return trim(strtolower(str_replace('* ', '', $line)));
    }
  }
  return FALSE;
}
